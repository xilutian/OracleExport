﻿namespace DataExport
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.datasource = new System.Windows.Forms.TextBox();
            this.username = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.BtnDatabaseconn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.exportPath = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.exportKey = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.exportWhere = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnexport = new System.Windows.Forms.Button();
            this.inputPath = new System.Windows.Forms.TextBox();
            this.btninput = new System.Windows.Forms.Button();
            this.btnInputSelectFile = new System.Windows.Forms.Button();
            this.btnexportSelectFolder = new System.Windows.Forms.Button();
            this.connectState = new System.Windows.Forms.Label();
            this.lbxtableinfo = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "数据库地址：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(310, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "用户名：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(538, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "密码：";
            // 
            // datasource
            // 
            this.datasource.Location = new System.Drawing.Point(95, 32);
            this.datasource.Name = "datasource";
            this.datasource.Size = new System.Drawing.Size(200, 21);
            this.datasource.TabIndex = 4;
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(354, 32);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(152, 21);
            this.username.TabIndex = 5;
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(573, 32);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(137, 21);
            this.password.TabIndex = 6;
            // 
            // BtnDatabaseconn
            // 
            this.BtnDatabaseconn.Location = new System.Drawing.Point(733, 30);
            this.BtnDatabaseconn.Name = "BtnDatabaseconn";
            this.BtnDatabaseconn.Size = new System.Drawing.Size(75, 23);
            this.BtnDatabaseconn.TabIndex = 7;
            this.BtnDatabaseconn.Text = "连接";
            this.BtnDatabaseconn.UseVisualStyleBackColor = true;
            this.BtnDatabaseconn.Click += new System.EventHandler(this.BtnDatabaseconn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(414, 291);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "导出路径：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 409);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "导入：";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // exportPath
            // 
            this.exportPath.Location = new System.Drawing.Point(486, 288);
            this.exportPath.Name = "exportPath";
            this.exportPath.Size = new System.Drawing.Size(251, 21);
            this.exportPath.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(414, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 14;
            this.label6.Text = "主键：";
            // 
            // exportKey
            // 
            this.exportKey.Location = new System.Drawing.Point(486, 90);
            this.exportKey.Name = "exportKey";
            this.exportKey.Size = new System.Drawing.Size(130, 21);
            this.exportKey.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(414, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 12);
            this.label7.TabIndex = 16;
            this.label7.Text = "where：";
            // 
            // exportWhere
            // 
            this.exportWhere.Location = new System.Drawing.Point(486, 141);
            this.exportWhere.Name = "exportWhere";
            this.exportWhere.Size = new System.Drawing.Size(280, 127);
            this.exportWhere.TabIndex = 17;
            this.exportWhere.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 203);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 18;
            this.label8.Text = "导出：";
            // 
            // btnexport
            // 
            this.btnexport.Location = new System.Drawing.Point(752, 324);
            this.btnexport.Name = "btnexport";
            this.btnexport.Size = new System.Drawing.Size(75, 23);
            this.btnexport.TabIndex = 19;
            this.btnexport.Text = "导出";
            this.btnexport.UseVisualStyleBackColor = true;
            this.btnexport.Click += new System.EventHandler(this.btnexport_Click);
            // 
            // inputPath
            // 
            this.inputPath.Location = new System.Drawing.Point(95, 406);
            this.inputPath.Name = "inputPath";
            this.inputPath.Size = new System.Drawing.Size(335, 21);
            this.inputPath.TabIndex = 20;
            // 
            // btninput
            // 
            this.btninput.Location = new System.Drawing.Point(540, 404);
            this.btninput.Name = "btninput";
            this.btninput.Size = new System.Drawing.Size(75, 23);
            this.btninput.TabIndex = 21;
            this.btninput.Text = "导入";
            this.btninput.UseVisualStyleBackColor = true;
            this.btninput.Click += new System.EventHandler(this.btninput_Click);
            // 
            // btnInputSelectFile
            // 
            this.btnInputSelectFile.Location = new System.Drawing.Point(451, 404);
            this.btnInputSelectFile.Name = "btnInputSelectFile";
            this.btnInputSelectFile.Size = new System.Drawing.Size(75, 23);
            this.btnInputSelectFile.TabIndex = 22;
            this.btnInputSelectFile.Text = "选择文件";
            this.btnInputSelectFile.UseVisualStyleBackColor = true;
            this.btnInputSelectFile.Click += new System.EventHandler(this.btnInputSelectFile_Click);
            // 
            // btnexportSelectFolder
            // 
            this.btnexportSelectFolder.Location = new System.Drawing.Point(752, 286);
            this.btnexportSelectFolder.Name = "btnexportSelectFolder";
            this.btnexportSelectFolder.Size = new System.Drawing.Size(75, 23);
            this.btnexportSelectFolder.TabIndex = 23;
            this.btnexportSelectFolder.Text = "选择文件";
            this.btnexportSelectFolder.UseVisualStyleBackColor = true;
            this.btnexportSelectFolder.Click += new System.EventHandler(this.btnexportSelectFolder_Click);
            // 
            // connectState
            // 
            this.connectState.AutoSize = true;
            this.connectState.ForeColor = System.Drawing.Color.Red;
            this.connectState.Location = new System.Drawing.Point(834, 36);
            this.connectState.Name = "connectState";
            this.connectState.Size = new System.Drawing.Size(41, 12);
            this.connectState.TabIndex = 24;
            this.connectState.Text = "未连接";
            // 
            // lbxtableinfo
            // 
            this.lbxtableinfo.FormattingEnabled = true;
            this.lbxtableinfo.ItemHeight = 12;
            this.lbxtableinfo.Location = new System.Drawing.Point(95, 90);
            this.lbxtableinfo.Name = "lbxtableinfo";
            this.lbxtableinfo.Size = new System.Drawing.Size(225, 268);
            this.lbxtableinfo.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(648, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(149, 12);
            this.label9.TabIndex = 26;
            this.label9.Text = "注：不写不会生成删除语句";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 552);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lbxtableinfo);
            this.Controls.Add(this.connectState);
            this.Controls.Add(this.btnexportSelectFolder);
            this.Controls.Add(this.btnInputSelectFile);
            this.Controls.Add(this.btninput);
            this.Controls.Add(this.inputPath);
            this.Controls.Add(this.btnexport);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.exportWhere);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.exportKey);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.exportPath);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnDatabaseconn);
            this.Controls.Add(this.password);
            this.Controls.Add(this.username);
            this.Controls.Add(this.datasource);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "Form1";
            this.Text = "导入导出";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox datasource;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Button BtnDatabaseconn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox exportPath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox exportKey;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox exportWhere;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnexport;
        private System.Windows.Forms.TextBox inputPath;
        private System.Windows.Forms.Button btninput;
        private System.Windows.Forms.Button btnInputSelectFile;
        private System.Windows.Forms.Button btnexportSelectFolder;
        private System.Windows.Forms.Label connectState;
        private System.Windows.Forms.ListBox lbxtableinfo;
        private System.Windows.Forms.Label label9;
    }
}

